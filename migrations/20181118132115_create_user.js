
exports.up = function(knex, Promise) {
    return knex.schema.createTable('users', (table)=>{
        table.increments()
        table.string('username').unique().notNullable()
        table.string('password').notNullable()
        table.string('email')
        table.string('name')
        table.json('savedData')
    })
};

exports.down = function(knex, Promise) {
    return knex.schema.dropTable('users')
};
