const Router = require('koa-router')
const router = new Router()
const Ctrl = require('../../controllers/api/auth')

router.post('/register', Ctrl.register)
router.post('/logout', Ctrl.logout)
router.post('/login', Ctrl.login)

router.get('/status', Ctrl.status)
router.get('/login', Ctrl.login)

module.exports = router.prefix('/api').routes()
