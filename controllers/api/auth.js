const knex = require('../../knexfile')

async function register(ctx){
    const received = ctx.request.body
    const user = await knex('users')
      .insert({
        username: received.username,
        password: received.hash,
        name: received.name,
        email: received.email,
      })
    return passport.authenticate('local', (err, user, info, status) => {
      if (user) {
        ctx.login(user);
        ctx.redirect('/auth/status');
      } else {
        ctx.status = 400;
        ctx.body = { status: 'error' };
      }
    })(ctx);
}
async function logout(ctx){
  if(ctx.isAuthenticated()){
    ctx.logout()
    ctx.redirect('auth/login')
  } else {
    ctx.body = {success:false}
    ctx.throw(401)
  }
}
async function login(ctx){
  return passport.authenticate('local', (err, user, info, status) => {
    if(user){
      ctx.login(user)
      ctx.redirect('auth/status')
    } else {
      ctx.status = 400;
      ctx.body = {status : 'error'}
    }
  })
}
async function status(ctx){
  if(ctx.isAuthenticated()){
    ctx.status = 200
  } else {
    ctx.status = 400 //berarti belum login
  }
}
module.exports = {
  register,
  logout,
  login,
  status
}