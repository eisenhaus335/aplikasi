const models = require('../models/')

async function findAll(ctx){
    const result = await models('product')
        .select('id', 'product_name', 'description')
    ctx.body = result
}
async function create(ctx){
    const newProduct = await models('product')
        .insert(ctx.request.body)
    ctx.body = newProduct
}
async function find(ctx){
    const result = await models('product')
        .select()
        .where('id', ctx.params.id)
    ctx.body = result
}
async function del(ctx){
    const result = await models('product')
        .select()
        .where('id', ctx.params.id)
        .del()
    ctx.body = result
}
module.exports = {
    findAll,
    create,
    find,
    del
}