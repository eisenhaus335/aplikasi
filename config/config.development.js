var config = require('./config.global');
 
config.env = 'development';
config.hostname = 'localhost'; 
config.database = {}
config.database.pg = {
    client : 'pg',
    connection : {
        host : 'localhost',
        user : 'admin',
        password : 'admin',
        database : 'database',
        charset: 'utf8'
    },
    migration : {
        directory: __dirname + '/knex/migrations'
    },
    seeds : {
        directory: __dirname + '/knex/seeds',
    },
}


module.exports = config;