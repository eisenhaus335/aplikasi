var config = require('./config.global');
 
config.env = 'development';
config.hostname = 'localhost'; 
config.database = {}
config.database.pg = {
    client : 'pg',
    connection : {
        host : 'postgres://lxovwzgtcyqyny:6fdfd417093adceef082c084282750fcc637db6d7620c0277931b68ecd67d9bd@ec2-184-72-221-2.compute-1.amazonaws.com:5432/dcn66bbsrcctd1',
        user : 'admin',
        password : 'admin',
        database : 'database',
        charset: 'utf8'
    },
    migration : {
        directory: __dirname + '/knex/migrations'
    },
    seeds : {
        directory: __dirname + '/knex/seeds',
    },
}


module.exports = config;